package com.ly.web.controller;

import com.ly.business.entities.User;
import com.ly.business.services.UserService;
import com.ly.web.controller.utilities.ResponseString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Shower on 2017/4/2 0002.
 */
@RestController
@RequestMapping(value = "/restful")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping(value = "/user", produces = "application/json")
    public ResponseString addUser(@RequestBody User user) {
        boolean rst = userService.add(user);
        return new ResponseString(rst + "");
    }

    @GetMapping(value = "/user/{id}", produces = "application/json")
    public User findUser(@PathVariable("id") long id) {
        return userService.find(id);
    }

    @GetMapping(value = "/user", produces = "application/json")
    public List<User> findAllUsers() {
        return userService.findAll();
    }

    @PostMapping(value = "/user/{id}", produces = "application/json")
    public ResponseString updateUser(@RequestBody User u) {
        User old = userService.update(u);
        return new ResponseString((old != null) + "");
    }

    @DeleteMapping(value = "/user/{id}", produces = "application/json")
    public ResponseString deleteUser(@PathVariable("id") long id) {
        User deleted = userService.delete(id);
        return new ResponseString((deleted != null) + "");
    }
}
